import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import Typography from "@material-ui/core/Typography";
import UserContext from "../contexts/UserContext";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
const styles = {
  card: {
    display: "flex",
    marginBottom: 100,
  },
  content: {
    padding: 25,
    objectFit: "cover",
  },
  MuiCardActionArea: {
    margin: 100,
  },
};

export default function EditPost() {
  const [posts, setPosts] = useState([]);
  const { jwt, user, setJwt, setUser } = useContext(UserContext);
  const [title, setTitle] = useState([]);
  const [content, setContent] = useState([]);

  const Addpost = () => {
    // console.log(jwt);
    axios
      .post(
        "https://strapi-crea.5ika.org/posts",

        { title: title, content: content, user: user },
        {
          headers: { Authorization: `Bearer ${jwt}` },
        }
      )
      .then((response) => {
        setPosts(response.data);
        // setUser(response.data);
      });
  };
  // console.log(test);
  // affich posts et les personne qui ont posté
  const onSubmit = (e) => {
    e.preventDefault();
    Addpost();
    setTitle("");
    setContent("");
  };
  return (
    <div>
      <h1>Editer un Post</h1>

      <Card>
        <CardActionArea>
          <CardContent>
            <Typography>
              <form>
                <TextField
                  id="title"
                  name="title"
                  type="text"
                  label=" Title"
                  // className={classes.textField}
                  // helperText={errors.name}
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  fullWidth
                />
                <TextField
                  id="content"
                  name="content"
                  type="text"
                  label=" Content"
                  // className={classes.textField}
                  // helperText={errors.name}
                  value={content}
                  onChange={(e) => setContent(e.target.value)}
                  fullWidth
                />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  onClick={onSubmit}
                >
                  Envoyer
                </Button>
              </form>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
}

import React from "react";
import { Grid } from "@material-ui/core";
import { Fragment } from "react";
//import des pages
import Users from "./Users";
import Post from "./Post";
import EditPost from "./Editpost";
// import Monprofile from "./Monprofile";

function Home() {
  return (
    <Fragment>
      <Grid container spacing={10}>
        <Grid item sm={9} xs={12}>
          <Post />
          <EditPost />
        </Grid>
        <Grid item sm={3} xs={12}>
          <Users />
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Home;

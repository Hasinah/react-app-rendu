import React, { useEffect, useContext, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import axios from "axios";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import UserContext from "../contexts/UserContext";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

const styles = {
  form: {
    textAlign: "center",
  },
  button: {
    marginTop: "50px",
  },
};

const Monprofile = () => {
  const [classes, setClasses] = useState("");
  const [lastname, setLastName] = useState("");
  const [firstname, setFirstName] = useState("");
  const [username, setUserName] = useState("");
  const [bio, setBio] = useState("");
  const [role, setRole] = useState("");

  const [email, setEmail] = useState("");
  const { jwt, user, setJwt, setUser, password, setPassword } = useContext(
    UserContext
  );
  // const [error, seteError] = useState("");

  let history = useHistory();
  // demande la date
  useEffect(() => {
    const jwtlocalstorage = localStorage.getItem("jwt");
    if (jwtlocalstorage) {
      setJwt(JSON.parse(jwtlocalstorage));
    }

    axios
      .get("https://strapi-crea.5ika.org/users/me", {
        headers: { Authorization: `Bearer ${jwt}` },
      })
      .then((response) => {
        console.log(response.data);
        setUserName(response.data.username);
        setEmail(response.data.email);
        setFirstName(response.data.firstname);
        setLastName(response.data.lastname);
        setBio(response.data.bio);
        setRole(response.data.role);
      });
  }, []);

  const getMonprofile = () => {
    console.log(user.id);
    axios
      .put(
        `https://strapi-crea.5ika.org/users/${user.id}`,
        {
          username: username,
          email: email,
          firstname: firstname,
          lastname: lastname,
          bio: bio,
          role: user.role,
        },
        {
          headers: { Authorization: `Bearer ${jwt}` },
        }
      )
      .then((response) => {
        setUser(response.data.user);
      });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    getMonprofile();
  };
  const Delete = () => {
    axios
      .delete(
        `https://strapi-crea.5ika.org/users/${user.id}`,

        {
          headers: { Authorization: `Bearer ${jwt}` },
        }
      )
      .then((response) => {
        setUser(response.data.user);
        localStorage.clear();
        window.location.reload(false);
        history.push("/login");
      });
  };

  //mon grid de main et asid avec un  espace entre  de 16px
  return (
    <div>
      <AppBar>
        <Toolbar className="nav-container">
          <Typography variant="h3" className={classes.pageTitle}>
            Monprofile
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <form>
            <TextField
              id="username"
              name="username"
              type="text"
              label=" User Name"
              className={classes.textField}
              // helperText={errors.name}
              value={username}
              onChange={(e) => setUserName(e.target.value)}
              fullWidth
            />
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              // className={classes.textField}
              // helperText={errors.email}
              // error={errors.email ? true : false}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
            />
            <TextField
              id="firstname"
              name="firstname"
              type="text"
              label=" First Name"
              className={classes.textField}
              // helperText={errors.name}
              value={firstname}
              onChange={(e) => setFirstName(e.target.value)}
              fullWidth
            />
            <TextField
              id="lastname"
              name="lastname"
              type="text"
              label="Last Name"
              className={classes.textField}
              // helperText={errors.name}
              value={lastname}
              onChange={(e) => setLastName(e.target.value)}
              fullWidth
            />

            <TextField
              id="bio"
              name="bio"
              type="text"
              label="Bio"
              className={classes.textField}
              // helperText={errors.password}
              value={bio}
              onChange={(e) => setBio(e.target.value)}
              fullWidth
            />

            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={onSubmit}
            >
              Enregister
            </Button>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={Delete}
            >
              Supprimer mon compte
            </Button>
            <Button color="inherit" component={Link} to="/">
              Retourner à la home
            </Button>

            <br />
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    </div>
  );
};
Monprofile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Monprofile);

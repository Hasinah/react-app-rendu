import React, { useContext, Component } from "react";
import AppBar from "@material-ui/core/AppBar"; //import seu
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import { useHistory, Link } from "react-router-dom";
import UserContext from "../contexts/UserContext";

// j'import mon styles de
// state = {
//   nameList: [{ id, identifier }],
// };
// const [name, setName] = useState("");
// const [password, setPassword] = useState("");

const Navebar = () => {
  const { setUsername, setPassword } = useContext(UserContext);
  let history = useHistory();
  const Logout = () => {
    setUsername(null);
    setPassword(null);
    history.push("/login");
    localStorage.clear();
    window.location.reload(false);
  };

  return (
    <AppBar>
      <Toolbar className="nav-container">
        <Button color="inherit" component={Link} to="/">
          Home
        </Button>
        <Button color="inherit" component={Link} to="/Monprofile">
          Mon profile
        </Button>
        <Link to="/post">Posts</Link>
        <Button
          className="link"
          variant="contained"
          color="primary"
          onClick={() => Logout()}
        >
          LOGOUT
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Navebar;

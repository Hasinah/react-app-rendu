import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import UserContext from "../contexts/UserContext";

const Users = ({ match }) => {
  const [posts, setPosts] = useState([]);
  const { jwt, user, users, setJwt, setUser, setUsers } = useContext(
    UserContext
  );
  let { userId } = match.params;
  useEffect(() => {
    axios
      .get(`https://strapi-crea.5ika.org/users/${userId}`, {
        headers: { Authorization: `Bearer ${jwt}` },
      })
      .then((response) => {
        setUsers(response.data);
      });
  }, [jwt, setUsers]);
  // console.log(test);
  // affich posts et les personne qui ont posté
  return (
    <div>
      <h1>Les Users</h1>

      <ul>
        <li>
          <p>{user.username}</p>
          <p>{user.title}</p>
          <p>{user.content}</p>
        </li>
      </ul>
    </div>
  );
};
export default Users;
